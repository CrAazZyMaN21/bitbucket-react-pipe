#!/usr/bin/env python

import os
import sys
import semver

__version__ = "1.0.0"
__author__ = 'CrAazZyMaN21'
__author_email__='info@craazzyman21.at'

try:
  ver = semver.VersionInfo.parse(sys.argv[1])

  major = str(ver.major)
  os.system('export DOCKER_TAG_MAJOR="major"')
  print ("Major: " + major)

  minor = str(major + '.' + str(ver.minor))
  os.system('export DOCKER_TAG_MINOR="minor"')
  print ("Minor: " + minor)

  patch = str(minor + '.' + str(ver.patch))
  os.system('export DOCKER_TAG_PATCH="patch"')
  print ("Patch: " + patch)

  if str(ver.prerelease) != 'None':
    pre_rel = str(patch + '-' + str(ver.prerelease))
    os.system('export DOCKER_TAG_PRE_RELEASE="pre_rel"')
    print ("Pre-Release: " + pre_rel)

    if str(ver.build) != 'None':
      build = str(pre_rel + '+' + str(ver.build))
      os.system('export DOCKER_TAG_BUILD="build"')
      print ("Build: " + build)

except AttributeError as err:
  print(err)

# Nginx Base Image
FROM nginx:mainline
# Add Maintainer Info
LABEL maintainer="CrAazZyMaN21 <hostmaster@craazzyman21.at>"
# Copy compiled folder into docker image
COPY ./build /usr/share/nginx/frontend/
# Remove default config from docker image
RUN rm /etc/nginx/conf.d/default.conf
# Copy react nginx config into docker image
COPY ./nginx/react.nginx /etc/nginx/conf.d/frontend.conf
